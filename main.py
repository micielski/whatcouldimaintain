#!/usr/bin/python
import subprocess
import requests
import lxml.html
from bs4 import BeautifulSoup


    
def get_orphaned_packages():
    r = requests.get("https://qa-reports.gentoo.org/output/maintainer-needed.html")
    orphaned_packages = []
    tree = lxml.html.fromstring(r.text)
    for elem in tree.xpath("//tr")[2:]:
        orphaned_packages.append(elem.text_content().strip().split()[0])
    return orphaned_packages

def get_outdated_guru_packages():
    outdated_packages = scrape_repology_page("https://repology.org/projects/?inrepo=gentoo_ovl_guru&outdated=on")
    while not len(more_outdated := scrape_repology_page(get_next_repology_page(outdated_packages))) < 200:
        outdated_packages += more_outdated
    return outdated_packages

def get_next_repology_page(outdated_packages):
    last_package = outdated_packages[-1]
    return f"https://repology.org/projects/{last_package}/?inrepo=gentoo_ovl_guru&outdated=on"

def scrape_repology_page(url):
    r = requests.get(url)
    packages = []
    tree = lxml.html.fromstring(r.text)
    for elem in tree.xpath("//td[contains(@class, 'text-nowrap')]"):
        # this star that is being stripped is no ordinary star. be warned
        package_name = elem.text_content().strip().rstrip("∗").split()[-1]
        packages.append(package_name)    
    return packages

    
def get_installed_packages():
    installed_packages_proc = subprocess.run(["equery", "--no-color", "l", "--format=$category/$name", "*"], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    return installed_packages_proc.stdout.decode("ascii").splitlines()


def get_installed_packages_name_only():
    installed_packages_proc = subprocess.run(["equery", "--no-color", "l", "--format=$name", "*"], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    return installed_packages_proc.stdout.decode("ascii").splitlines()

if __name__ == "__main__":
    print("Gentoo packages without maintainer that you have installed:")
    orphaned_packages = get_orphaned_packages()
    installed_packages = get_installed_packages()
    for orphan in orphaned_packages:
        if orphan in installed_packages:
            print(orphan)
    print("GURU outdated packages that you have installed:")
    outdated_guru_packages = get_outdated_guru_packages()
    installed_packages_name_only = get_installed_packages_name_only()
    for outdated in outdated_guru_packages:
        if outdated in installed_packages_name_only:
            print(outdated)
    